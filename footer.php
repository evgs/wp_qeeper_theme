<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>


<div class="container-fluid footer-fluid">
    <div class="container">
        <footer>
            <div class="all">
                <div class="row">
                    <div class="up clearfix">
                        <div class="col1">
                            <div class="title">
                                <a href="index.php">Страхование</a>
                            </div>
                            <ul>
                                <li>
                                    <a href="http://qeeper.ru/index.php">Туристическая страховка</a>
                                </li>
                                <li>
                                    <a href="http://qeeper.ru/osago.php#calculate">ОСАГО</a>
                                </li>
                                <li>
                                    <a href="http://qeeper.ru/osago.php#calculate">Зеленая карта</a>
                                </li>
                                <li>
                                    <a href="http://qeeper.ru/index.php">Мобильное приложение</a>
                                </li>
                            </ul>
                        </div>
                        <!---end col1-->
                        <div class="col2">
                            <div class="title">
                                <a href="http://qeeper.ru/articles.php">Полезная информация</a>
                            </div>
                            <ul>
                                <li>
                                    <a href="http://qeeper.ru/articles.php">Новости</a>
                                </li>
                                <li>
                                    <a href="http://qeeper.ru/articles.php">Статьи</a>
                                </li>
                                <li>
                                    <a href="http://qeeper.ru/articles.php">Советы экспертов</a>
                                </li>
                            </ul>
                        </div>
                        <!---end col2-->
                        <div class="col3">
                            <div class="title">
                                <a href="http://qeeper.ru/zarabotok.php">Партнерская программа</a>
                            </div>
                            <ul>
                                <li>
                                    <a href="http://qeeper.ru/zarabotok.php#cond">Превосходные условия для заработка</a>
                                </li>
                                <li>
                                    <a href="http://qeeper.ru/zarabotok.php#income">Расчет дохода</a>
                                </li>
                                <li>
                                    <a href="http://qeeper.ru/zarabotok.php#instruments">Инструменты для заработка</a>
                                </li>
                            </ul>
                        </div>
                        <!---end col3-->
                        <div class="col4">
                            <div class="title">
                                <a href="http://qeeper.ru/about.php">О нас</a>
                            </div>
                            <ul>
                                <li>
                                    <a href="http://qeeper.ru/about.php#history">История проекта</a>
                                </li>
                                <li>
                                    <a href="http://qeeper.ru/about.php#pros">Преимущества Qeeper</a>
                                </li>
                            </ul>
                        </div>
                        <!---end col4-->
                        <div class="col5">
                            <div class="title">
                                <a href="http://qeeper.ru/contact.php">Контакты</a>
                            </div>
                            <ul>
                                <li>
                                    <a href="http://qeeper.ru/contact.php#write">Напишите нам</a>
                                </li>
                                <li>
                                    <a href="http://qeeper.ru/contact.php#team">Наша команда</a>
                                </li>
                            </ul>
                        </div>
                        <!---end col5-->
                    </div>
                    <!--end up-->
                </div> 
                <div class="row">
                    <div class="copy">
                        <a href="javascript:" id="ua" class="ua">Пользовательское соглашение</a>
                        <a href="javascript:" id="ua" class="ua">Оферта. Партнерская программа.</a>
                        <small>* — п.3.1 пользовательского соглашения
                        </small>

                    </div>
                    <!--end copy-->
                    <div class="dws">
                        <a href="https://itunes.apple.com/us/app/qeeper-osago-onlajn/id1093552292?l=ru&amp;ls=1&amp;mt=8"><img alt="" src="/img/app.png"></a>
                        <a href="https://play.google.com/store/apps/details?id=pro.topdigital.insurance"><img alt="" src="/img/googleplay.png"></a>
                    </div>
                    <!--end dws-->
                    <div class="cards-link">
                        <img src="/img/footer-card-icons.png" alt="">
                    </div>
                    <div class="socs">
                        <a id="facebook_go" href="https://www.facebook.com/%D0%9C%D0%BE%D0%B1%D0%B8%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5-%D0%B0%D0%B2%D1%82%D0%BE%D1%81%D1%82%D1%80%D0%B0%D1%85%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-1059208094140378/"><img alt="" src="/img/ss1.png"></a>
                        <a id="instagramm_go" href="https://www.instagram.com/qeeper_ru/"><img alt="" src="/img/ss2.png"></a>
                        <a id="vk_go" href="https://vk.com/tripstartup"><img alt="" src="/img/ss3.png"></a>

                    </div>
                    <!--end socs-->
                </div>
            </div>
            <!--end all-->

        </footer>
    </div>
</div>
</div><!-- #page -->
<?php wp_footer(); ?>
<!-- Bootstrap core JavaScript -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/js/jquery.min.js"></script>
<script src="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/js/bootstrap.min.js"></script>
<!-- owl -->
<script src="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/js/owl.carousel.js"></script>
<script>
    $('.row_owl').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        dots: true,
        responsive: {
            1000: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    })
</script>
<script src="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/js/isotope.pkgd.min.js"></script>
<script src="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/js/imagesloaded.pkgd.js"></script>
<script>
    var $grid = $('.cats_news_items').imagesLoaded(function () {
        // init Isotope after all images have loaded
        $grid.isotope({
            itemSelector: '.cats_news_item',
            percentPosition: true,
            masonry: {
                columnHeight: '.cats_news_item-sizer'
            }
        });
    });
</script>
<script>
    window.onscroll = function () {
        console.log($(this).scrollTop());
        if ($(this).scrollTop() < 437/* && $(this).scrollTop() < 5290*/) {
            $(".cats_news_in_header").hide();
        } else {
            $(".cats_news_in_header").show();
        }
    }
</script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
