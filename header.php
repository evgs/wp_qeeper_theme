<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Qeeper
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php /*wp_head(); */ ?>

    <link href="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/css/bootstrap.min.css?v=2" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/css/style.css?v=2" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!--[if lt IE 9]>
    <script
        src="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->
    <!-- owl -->
    <link href="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/css/owl.carousel.css" rel="stylesheet">
    <link href="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/css/owl.theme.default.css" rel="stylesheet">

    <script src="http://qeepertest.s04.wh1.su/wp-content/themes/qeeper/js/ie-emulation-modes-warning.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]-->
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <div class="container container-fix-top">
        <header>
            <!--<div class="block-logo">
                <div class="content">
                    <a class="link" href="http://qeeper.ru/" title="Мобильное страхование">
                        <i class="logo"></i>
                        <span class="logo-title">Мобильное страхование</span>
                    </a>

                </div>
            </div>-->
            <div class="row">
                <div class="col-xs-2 col-xs-no_m1"><a href="/"><img src="/img/temp_logo.png" alt=""></a>
                </div>
                <div class="col-xs-9 hidden-xs">
                    <div class="cats_news cats_news_in_header text-center">
                        <a href="555" class="active">Все новости</a>
                        <span class="cats_news_split">|</span>
                        <a href="555">Советы по страхованию</a>
                        <span class="cats_news_split">|</span>
                        <a href="555">Советы путешественникам</a>
                        <span class="cats_news_split">|</span>
                        <a href="555">Обзоры</a>
                        <span class="cats_news_split">|</span>
                        <a href="555">Новое</a>
                        <span class="cats_news_split">|</span>
                        <a href="555">Экспертное мнение</a>
                    </div>
                </div>
                <div class="col-xs-1 col-xs-no_m2 relat">
                    <img class="top_menu_icon" src="/img/temp_menu.png" alt="">

                    <div class="top_menu abs">
                        <div class="top_menu_item0"></div>
                        <div class="top_menu_item">
                            <a href="555">ОСАГО</a>
                        </div>
                        <div class="top_menu_item">
                            <a href="555">Туристическая страховка</a>
                        </div>
                        <div class="top_menu_item">
                            <a href="555">Партнерская программа</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <!-- #masthead -->

    <?php
    // If a regular post or page, and not the front page, show the featured image.
    if (has_post_thumbnail() && (is_single() || (is_page() && !twentyseventeen_is_frontpage()))) :
        echo '<div class="single-featured-image-header">';
        the_post_thumbnail('twentyseventeen-featured-image');
        echo '</div><!-- .single-featured-image-header -->';
    endif;
    ?>
