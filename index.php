<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap" style="display: none;">
	<?php if ( is_home() && ! is_front_page() ) : ?>
		<header class="page-header">
			<h1 class="page-title"><?php single_post_title(); ?></h1>
		</header>
	<?php else : ?>
	<header class="page-header">
		<h2 class="page-title"><?php _e( 'Posts', 'twentyseventeen' ); ?></h2>
	</header>
	<?php endif; ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/post/content', get_post_format() );

				endwhile;

				the_posts_pagination( array(
					'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
				) );

			else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->



    <div class="container-fluid slider-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="row_owl owl-carousel owl-theme relat">
                    <div class="item_owl"><img src="img/slider.png" alt="">

                        <div class="container relat">
                            <div class="item_owl_text abs">
                                <div class="cats_news_header inline">Советы по страхованию
                                </div>
                                <h4>Массовое страхование жилья в России: тенденции</h4>
                            </div>
                        </div>
                    </div>
                    <div class="item_owl"><img src="img/slider.png" alt="">

                        <div class="container relat">
                            <div class="item_owl_text abs">
                                <div class="cats_news_header inline">Советы по страхованию
                                </div>
                                <h4>Массовое страхование жилья в России: тенденции</h4>
                            </div>
                        </div>
                    </div>
                    <div class="item_owl"><img src="img/slider.png" alt="">

                        <div class="container relat">
                            <div class="item_owl_text abs">
                                <div class="cats_news_header inline">Советы по страхованию
                                </div>
                                <h4>Массовое страхование жилья в России: тенденции</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="cats_news_head text-center">Последние новости</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="cats_news text-center">
                    <a href="555" class="active">Все новости</a>
                    <span class="cats_news_split">|</span>
                    <a href="555">Советы по страхованию</a>
                    <span class="cats_news_split">|</span>
                    <a href="555">Советы путешественникам</a>
                    <span class="cats_news_split">|</span>
                    <a href="555">Обзоры</a>
                    <span class="cats_news_split">|</span>
                    <a href="555">Новое</a>
                    <span class="cats_news_split">|</span>
                    <a href="555">Экспертное мнение</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="cats_news_items relats">
                    <div class="cats_news_item-sizer"></div>
                    <div class="cats_news_item relat"><img src="img/news_02.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_04.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_06.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_09.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_14.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_15.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_02.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_04.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_06.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_09.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_14.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                    <div class="cats_news_item relat"><img src="img/news_15.jpg" alt="">
                        <div class="cats_news_header abs">Советы по страхованию
                        </div>
                        <div class="cats_news_text abs">text</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
